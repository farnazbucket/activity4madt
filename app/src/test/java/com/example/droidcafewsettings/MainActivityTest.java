package com.example.droidcafewsettings;

import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.AppCompatImageView;
import android.view.Menu;
import android.widget.Button;

import static org.hamcrest.CoreMatchers.equalTo;
import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNotNull;
import static junit.framework.Assert.assertTrue;
import static org.junit.Assert.assertThat;

import org.junit.Before;
import org.junit.Test;

import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.Shadows;
import org.robolectric.annotation.Config;
import org.robolectric.shadow.api.Shadow;
import org.robolectric.shadows.ShadowActivity;
import org.robolectric.shadows.ShadowToast;

import android.support.v7.widget.Toolbar;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;

@RunWith(RobolectricTestRunner.class)

public class MainActivityTest {
    

    private MainActivity activity;
    private OrderActivity orderActivity;


    @Before
    public void setUp() throws Exception {
        activity = Robolectric.buildActivity(MainActivity.class)
                .create()
                .resume()
                .get();
    }

        @Test
    public void activityIsNotNull() throws Exception {
        assertNotNull(activity);

    }
    @Test
    public void froyotest() throws Exception {
        assertNotNull(activity);
        // clicking on froyo image view
        ImageView froyo = (ImageView) activity.findViewById(R.id.froyo);

        froyo.performClick();
        // checking to see if the toast message is same as expected output

        assertThat(ShadowToast.getTextOfLatestToast().toString(), equalTo("You ordered a FroYo."));

    }
    @Test

    public void shoppingcarttest() throws Exception {
// going to order activioty from main to store the data of the user
        FloatingActionButton fab = (FloatingActionButton) activity.findViewById(R.id.fab);
        fab.performClick();
// starting the order activity to get data from it
        orderActivity = Robolectric.buildActivity(OrderActivity.class)
                .create()
                .resume()
                .get();
    // setting the name edit text
        EditText name = (EditText) orderActivity.findViewById(R.id.name_text);
        name.setText("farnaz");

//       setting the address edit text
        EditText address = (EditText) orderActivity.findViewById(R.id.address_text);
        address.setText("151 herthford");
        // setting the phone edit text
        EditText Phone = (EditText) orderActivity.findViewById(R.id.phone_text);
        Phone.setText("6478184742");
        // setting the note edit text
        EditText note = (EditText) orderActivity.findViewById(R.id.note_text);
        note.setText("Extra nuts not so cold");
// selecting a radio button option
        RadioButton sameday = (RadioButton) orderActivity. findViewById(R.id.sameday);
        sameday.setChecked(true);
// saving the details by clicking on save button
        Button save = (Button) orderActivity.findViewById(R.id.saveButton);
        save.performClick();
// going back to previous activity
        orderActivity.onBackPressed();
        // now clicking on the shopping cart button to check if the data is saved
        fab.performClick();

// asserting and compairing the actual and expected outputs
        assertThat(name.getText().toString(),equalTo("farnaz"));
        assertThat(address.getText().toString(),equalTo("151 herthford"));
        assertThat(Phone.getText().toString(),equalTo("6478184742"));
        assertThat(note.getText().toString(),equalTo("Extra nuts not so cold"));
        assertTrue(sameday.isChecked());



    }






}






